<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('frontendFiles', dirname(dirname(__DIR__)) . '/../golfo.com.ua');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('frontendWebroot', 'http://golfo.com.ua/');
Yii::setAlias('backendWebroot', 'http://admin.golfo.com.ua/');