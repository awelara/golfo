<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "params".
 *
 * @property integer $id
 * @property string $type
 * @property string $value
 */
class Params extends \yii\db\ActiveRecord
{

    const TYPE_COLOR='color';
    const TYPE_SIZE='size';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'params';
    }

    public function getTypes()
    {
        return [
            self::TYPE_COLOR=>self::TYPE_COLOR,
            self::TYPE_SIZE=>self::TYPE_SIZE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','label', 'value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'value' => 'Value',
            'label' => 'Label',
        ];
    }
}
