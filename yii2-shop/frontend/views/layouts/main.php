<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\models\Order;
use frontend\widgets\Alert;
use \yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="_NkCrl2WzfkqLMyUUKTZEsdxZgWqY7xsX3wg4BfhLtE" />
        <meta name='yandex-verification' content='7030138d661b6427' />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--    <script src="http://code.jquery.com/jquery-latest.js"></script>-->
    <!--    <script src="/js/bootstrap.min.js"></script>-->
    <!--    <script src="/js/script.js"></script>-->
    <!--    <script type="text/javascript" src="/js/modernizr.custom.28468.js"></script>-->

<!--        <link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="screen" />-->
<!--        <link href="/css/bootstrap.css" rel="stylesheet" media="screen">-->
<!--        <link href="/css/bootstrap-responsive.css" rel="stylesheet" media="screen">-->
<!--        <link href="/css/css.css" rel="stylesheet" media="screen">-->
<!--        <link rel="stylesheet" type="text/css" href="/css/style2.css" />-->
<!--        <link rel="stylesheet" href="/css/animations.css">-->



    </head>
    <body>
        <?php $this->beginBody() ?>
        <header>
            <div class="container">
                <div class="header">
                    <div class="tel span4">
                        <p class="befor">(0482) 31-31-55</p>
                        <p>(048) 729-80-00</p>

<!--                                        <a href="#">обратный звонок</a>-->
                    </div>
                    <div class="logo span4">
                        <a href="/"><img src="/img/logo.png"/></a>
                    </div>
                    <div class="cart span4">
                        <div class="cart_item">
                            <br>
                            <a href="#myModal" role="button" data-target="#myModal" data-toggle="modal">Корзина</a>
                            <p><?= count(\Yii::$app->cart->getPositions()) ?> товара</p>
                            <p><?= \Yii::$app->cart->cost ?> грн</p>
                        </div>
                        <div class="search">
                            <form class="form-search">
                                <br>
        <!--                        <input type="text" value="поиск по сайту" class="input-medium search-query">-->
                                <!--                        <button type="submit" class="btn">Найти</button>-->
                            </form>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </header>
        <div class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="<?= \yii\helpers\Url::toRoute(['/']) ?>">Главная</a></li>
                            <li><a href="<?= \yii\helpers\Url::toRoute(['site/about']) ?>">О компании</a></li>
                            <li><a href="<?= \yii\helpers\Url::toRoute(['site/delivery']) ?>">Условия доставки</a></li>
                            <li><a href="<?= \yii\helpers\Url::toRoute(['site/contacts']) ?>">Контакты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>



        <?= $content ?>
        <br>
        <div class="about">
            <div class="container">
                <div class="about1 span6">
                    <h2>О нашем производстве</h2>
        <!--            <h2><span>Коротко о компании</span></h2>-->
                    <!--            <h3>Lorem Ipsum - це текст-"риба"</h3>-->

                    <p>
                        Продукция торговой марки «Golfo» выпускается предприятием ООО «Николаевская швейная фабрика». Профильным направлением деятельности швейного цеха является производство линий женской, мужской и детской одежды. Кроме того, цех специализируется на пошиве спецодежды, постельного и нижнего белья. Фабрика оснащена современным высокотехнологичным оборудованием и укомплектована исключительно  профессиональным, компетентным персоналом. В процессе изготовления текстильной продукции используется только сертифицированное сырьё высокого качества.
                    </p>
                    <a href="<?= \yii\helpers\Url::toRoute(['site/about']) ?>">Подробнее</a>
                    <div class="clear"></div>
                </div>
                <div class="about2 span6">
                    <img src="/img/socks.jpg"/>
                </div>
            </div>
        </div>
        <div class="bottom_seo">
            <div class="container">
                <h2>Интернет-магазин Одежды</h2>
                <div class="container">
                    <div class="seo_block1">
                        <ul>
                            <li>Golfo.com.ua – это интернет-магазин качественной одежды украинского производства. Здесь Вы можете купить мужскую, женскую, а также детскую одежду по низким ценам с удобной доставкой по Украине. </li>
                            <li>Каталог нашего интернет-магазина представлен широким ассортиментом спортивной одежды по демократичным ценам.</li>

                        </ul>
                        <ul>
                            <li>Модельный ряд онлайн-магазина golfo.сom.ua постоянно пополняется новым ассортиментом одежды и аксессуарами.</li>
                            <li>«Golfo» – это возможность купить одежду оптом и в розницу с оптимальным соотношением низкой цены и высокого качества продукции.  </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="footer_item1 span4">
                    <ul>
                        <li ><a href="<?= \yii\helpers\Url::toRoute(['/']) ?>">Главная</a></li>
                        <li><a href="<?= \yii\helpers\Url::toRoute(['site/about']) ?>">О компании</a></li>
                        <li><a href="<?= \yii\helpers\Url::toRoute(['site/delivery']) ?>">Условия доставки</a></li>
                        <li><a href="<?= \yii\helpers\Url::toRoute(['site/contacts']) ?>">Контакты</a></li>
                    </ul>
                    <div class="search">
                        <form class="form-search">
        <!--                    <input type="text" value="поиск по сайту" class="input-medium search-query">-->
                            <!--                    <button type="submit" class="btn">Найти</button>-->
                        </form>
                    </div>
                </div>
                <div class="footer_item1 span4">
                    <div class="tel">
                        <p class="befor">(0482) 31-31-55</p>
                        <p>(048) 729-80-00</p>
                        <!--                <a class="helpful6" href="#">обратный звонок</a>-->
                    </div>
                </div>
                <div class="footer_item1 span4">
                    <div class="maps">
                        <p class="befor_maps">Украина, Одесса, <br> Гагаринское плато 5/3 </p>
        <!--                <p class="befor_mail"><a href="#">info@golfo.com.ua</a></p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="clear"></div>

        <p class="helpful3" id="back-top"><a href="#top"><span></span></a></p>

        <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Оформление заказа</h3>
            </div>
            <?php
            /* @var $form ActiveForm */
            $form = ActiveForm::begin([
                        'id' => 'order-form',
                        'options' => ['class' => 'form-horizontal'],
                        'action' => \yii\helpers\Url::toRoute(['cart/order']),
//    'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'validateOnSubmit' => true,
            ]);
            $order = new Order();
            ?>
            <div class="modal-body" >

                <div id="cartFormItems"></div>

                <?if(count(\Yii::$app->cart->getPositions())>0) {?>
                <p>Для оформления заказа необходимо заполнить форму:</p>
                <p class="star_red">Поля, помеченные звездочкой (<span>*</span>) обязательны для заполнения! </p>

                <?= $form->field($order, 'notes')->label('Ваше имя : ') ?>
                <?= $form->field($order, 'email')->label('<span>*</span>Е-mail : ') ?>
                <?= $form->field($order, 'phone')->label('<span>*</span>Телефон : ') ?>

                <div class="modal-footer">
                    <a  data-dismiss="modal" href="#">Продолжить покупки</a>
                    <?= Html::submitButton('Оформить заказ', ['class' => 'btn issue']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
            <?}?>
        </div>


        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
            { (i[r].q=i[r].q||[]).push(arguments)}

                ,i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-63674935-1', 'auto');
            ga('send', 'pageview',
                {'dimension1', '12151555'}

            );

        </script>



        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter30684473 = new Ya.Metrika({id:30684473,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/30684473" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>