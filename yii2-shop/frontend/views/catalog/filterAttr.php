<?php
/**
 * Created by PhpStorm.
 * User: awelara
 * Date: 27.05.15
 * Time: 16:38
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
    <ul>

        <?  foreach($res as $c){
            if($changeAttr=='color'){
            ?><li><a id="product-<?=$changeAttr?> option[value='<?=$c->id?>']" dataid="<?=$c->id?>" style="background: <?=mb_strtolower($c->value)?>;" class="product-<?=$changeAttr?>" ></a></li>
        <?}
            else{?>
                        <li><a class="product-<?=$changeAttr?>" id="product-size option[value='<?=$c->id?>']"  dataid="<?=$c->id?>" ><?=mb_strtolower($c->value)?></a></li>
            <?}
        } ?>
    </ul>
    <div class="form-group field-product-<?=$changeAttr?> required">
    <select id="product-<?=$changeAttr?>" class="form-control" name="Product[<?=$changeAttr?>]" style="display:block">
        <option value="">Select <?=$changeAttr?></option>
<? foreach($res as $c){
    ?>
    <option value="<?=$c->id?>"><?=$c->label?></option>
<?
}?>
    </select>
        <div class="help-block"></div>
    </div>
    <div class="clear"></div>
