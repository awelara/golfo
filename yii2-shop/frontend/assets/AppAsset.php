<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/bootstrap-responsive.css',
        'css/flexslider.css',
        'css/animations.css',
        'css/style2.css',
        'css/css.css',
    ];
    public $js = [
        'http://code.jquery.com/jquery-latest.js',
        'js/jquery.flexslider.js',
        'js/script.js',
        'js/bootstrap.min.js',
        'js/modernizr.custom.28468.js',
        'js/jquery.cslider.js',
//        '',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\web\JqueryAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
