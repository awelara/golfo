<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets;

use common\models\Category;
use yii\helpers\Html;
/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', 'This is the message');
 * \Yii::$app->getSession()->setFlash('success', 'This is the message');
 * \Yii::$app->getSession()->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class MenuBase extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */

    /**
     * @var array the options for rendering the close button tag.
     */

    public function init()
    {
        parent::init();

        $categories = Category::find()/*->where(['parent_id'=>0])*/->indexBy('id')->orderBy('id')->all();
        $menu=$this->getMenuItems($categories, isset($category->id) ? $category->id : null);

        foreach($menu as $m)
        {
            $count++;
            ?>
            <h3 class="kids_clothing v<?=$count?>">
                <a href="<?=\yii\helpers\Url::toRoute($m['url'])?>"><?=Html::encode($m['label'])?></a>
            </h3>
            <ul>
                <? if($m['items']){
                    foreach($m['items'] as $i){
                        ?>
                        <li>
                            <a href="<?=\yii\helpers\Url::toRoute($i['url'])?>"><?=$i['label']?></a>
                        </li>
                    <?}?>

                <?}?>

            </ul>
        <?

        }


    }

    /**
     * @param Category[] $categories
     * @param int $activeId
     * @return array
     */
    private function getMenuItems($categories, $activeId = null)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            if ($category->parent_id === 0) {
                $menuItems[$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/list', 'id' => $category->id],
                ];
            } else {
                $this->placeCategory($category, $menuItems, $activeId);
            }
        }
        return $menuItems;
    }

    /**
     * Places category menu item into menu tree
     *
     * @param Category $category
     * @param $menuItems
     * @param int $activeId
     */
    private function placeCategory($category, &$menuItems, $activeId = null)
    {
        foreach ($menuItems as $id => $navLink) {
            if ($category->parent_id === $id) {
                $menuItems[$id]['items'][$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/list', 'id' => $category->id],
                ];
                break;
            } elseif (!empty($menuItems[$id]['items'])) {
                $this->placeCategory($category, $menuItems[$id]['items']);
            }
        }
    }

    /**
     * Returns IDs of category and all its sub-categories
     *
     * @param Category[] $categories all categories
     * @param int $categoryId id of category to start search with
     * @param array $categoryIds
     * @return array $categoryIds
     */
    private function getCategoryIds($categories, $categoryId, &$categoryIds = [])
    {
        foreach ($categories as $category) {
            if ($category->id == $categoryId || $category->parent_id == $categoryId) {
                $categoryIds[] = $category->id;
            }
            if (isset($categories[$categoryId]['items'])) {
                foreach ($categories[$categoryId]['items'] as $subCategoryId => $subCategory)
                    $this->getCategoryIds($categories, $subCategoryId, $categoryIds);
            }
        }
        return $categoryIds;
    }

}
