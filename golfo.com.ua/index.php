<?php

//phpinfo();
//exit;
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../yii2-shop/vendor/autoload.php');
require(__DIR__ . '/../yii2-shop/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../yii2-shop/common/config/bootstrap.php');
require(__DIR__ . '/../yii2-shop/frontend/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../yii2-shop/common/config/main.php'),
    require(__DIR__ . '/../yii2-shop/common/config/main-local.php'),
    require(__DIR__ . '/../yii2-shop/frontend/config/main.php'),
    require(__DIR__ . '/../yii2-shop/frontend/config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();
