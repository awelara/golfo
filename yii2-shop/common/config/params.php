<?php
return [
    'adminEmail' => 'CallCenter@tavriav.com.ua',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
