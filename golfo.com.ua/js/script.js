$(window).load(function () {
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 107,
        itemMargin: 0,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function (slider) {
            $('body').removeClass('loading');
        }
    });
});


$(document).ready(function () {
    // ���������/��������� ������ #back-top
    $(function () {
        // ������ ������ #back-top
        $("#back-top").hide();

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $("#back-top").fadeIn();
            } else {
                $("#back-top").fadeOut();
            }
        });

        // ��� ����� �� ������ ������ ����������� �����
        $("#back-top a").click(function () {
            $("body,html").animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
});

$(document).ready(function () {
    $(window).scroll(function () {
        $('.helpful2').each(function () {
            var imagePos = $(this).offset().top;
            var imagePosHeight = imagePos + $(this).height();
            var topOfWindow = $(window).scrollTop();
            if (imagePos > topOfWindow + $(this).height())
                $(this).removeClass("animated ");
            if (imagePos < topOfWindow + $(window).height() / 1.3) {
                $(this).addClass("animated  fadeIn");
            }
            if (imagePosHeight < topOfWindow)
                $(this).removeClass("animated")
        });
    });
});

$(document).ready(function () {
    $(window).scroll(function () {
        $('.helpful3').each(function () {
            var imagePos = $(this).offset().top;
            var imagePosHeight = imagePos + $(this).height();
            var topOfWindow = $(window).scrollTop();
            if (imagePos > topOfWindow + $(this).height())
                $(this).removeClass("animated ");
            if (imagePos < topOfWindow + $(window).height() / 1) {
                $(this).addClass("animated slideLeft");
            }
            if (imagePosHeight < topOfWindow)
                $(this).removeClass("animated")
        });
    });
});

$(document).ready(function () {
    $(window).scroll(function () {
        $('.helpful4').each(function () {
            var imagePos = $(this).offset().top;
            var imagePosHeight = imagePos + $(this).height();
            var topOfWindow = $(window).scrollTop();
            if (imagePos > topOfWindow + $(this).height())
                $(this).removeClass("animated ");
            if (imagePos < topOfWindow + $(window).height() / 1) {
                $(this).addClass("animated slideDown");
            }
            if (imagePosHeight < topOfWindow)
                $(this).removeClass("animated")
        });
    });
});

$(document).ready(function () {
    $(window).scroll(function () {
        $('.helpful6').each(function () {
            var imagePos = $(this).offset().top;
            var imagePosHeight = imagePos + $(this).height();
            var topOfWindow = $(window).scrollTop();
            if (imagePos > topOfWindow + $(this).height())
                $(this).removeClass("animated ");
            if (imagePos < topOfWindow + $(window).height() / 1) {
                $(this).addClass("animated pulse");
            }
            if (imagePosHeight < topOfWindow)
                $(this).removeClass("animated")
        });
    });
});

$(document).ready(function () {

$('#myModal').on('show', function () {
    $( "#cartFormItems" ).load( "/cart/order")
})

$('#myModal').on('hidden', function () {
    location.reload();
})



    $('.nav li a').each(function(){
        var active=$(this).attr('href')
        if('http://golfo.com.ua'+active==location.href)
        {
            $(this).parent().attr('class','active')
        }
    })



})