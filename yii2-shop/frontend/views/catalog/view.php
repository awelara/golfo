<?php
/* @var $this yii\web\View */
use himiklab\thumbnail\EasyThumbnailImage;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>


<br>
<div class="clear"></div>
</div>


<div class="card_product">
<div class="container">
<div class="block_left span3">

        <?

        echo \frontend\widgets\MenuBase::widget();
        ?>
    <div class="clear"></div>


</div>
<div class="block_right span9">
<h1><?=$product->name?></h1>
<div class="main_left span5">

    <div id="main" role="main">
        <section class="slider2">
            <div id="slider" class="flexslider">
                <ul class="slides">

                    <?
                     foreach($product->images as $image){

                         echo  ' <li>'.EasyThumbnailImage::thumbnailImg(
                             $image->getPath(),
                             500,
                             700,
                             EasyThumbnailImage::THUMBNAIL_INSET,
                             ['alt' => $image->id]
                         ).'</li>';

                     }


                    ?>

                </ul>
            </div>
            <div id="carousel" class="flexslider">
                <ul class="slides">

                    <?
                    foreach($product->images as $image){

                        echo  ' <li>'.EasyThumbnailImage::thumbnailImg(
                                $image->getPath(),
                                109,
                                76,
                                EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                                ['alt' => $image->id]
                            ).'</li>';
                    }
                    ?>
                </ul>
            </div>
        </section>

    </div>
</div>
<div class="main_right span3">
    <div class="top_nav">
        <?if($prev->id){?>
        <a href="<?=\yii\helpers\Url::toRoute(['catalog/view','id'=>$prev->id])?>"><< Предыдущий товар </a>
        <?}?>|
        <?if($next->id){?>
        <a href="<?=\yii\helpers\Url::toRoute(['catalog/view','id'=>$next->id])?>">Следующий товар >></a>
        <?}?>
    </div>
    <div id="prodData">
        <div class="cart_right">
            <div class="price_right"><?=round($product->product->price,2)?> грн</div>
            <?php

            $form = ActiveForm::begin([
                'id'=>'form-filter',
//                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'validateOnSubmit'=>true,
            ]);
            ?>
            <div id="filters">
            <?=include('filterAttr2.php')?>
            </div>
            <?php ActiveForm::end(); ?>
            <script>

            </script>

            <?
            $url=\yii\helpers\Url::toRoute(['catalog/filter','id'=>$product->id]);
            $script = <<< JS
  renderFilters()
                $(document).on('click', ".product-color", function(){
                    $('.select_color ul li a').each(function(){
                        $(this).removeClass('border')
                    })
                    if($('#'+this.id).prop('selected')==true)
                    {
                        $('#'+this.id).prop('selected', false);
                        $(this).removeClass('border')
                    }
                    else
                    {
                        $('#'+this.id).prop('selected', true);
                        $(this).addClass('border')
                    }
                    renderFilters()
                });

                $(document).on('click', ".product-size", function(){
                    $('.select_size ul li a').each(function(){
                        $(this).removeClass('border-size')
                    })

                    if($('#'+this.id).prop('selected')==true)
                    {
                        $('#'+this.id).prop('selected', false);

                        $(this).removeClass('border-size')
                    }
                    else
                    {
                        $('#'+this.id).prop('selected', true);
                        $(this).addClass('border-size')
                    }
                    renderFilters()
                });

                function renderFilters()
                {

                        $.ajax({
                            url: "$url",
                            type: "POST",
                            data: $('#form-filter').serialize(),
                            success: function(data){
//                                alert(data)
                                if(data)
                                {
                                    $('#filters').html(data)
                                }

                            }
                        });

                }
JS;
            $this->registerJs($script, View::POS_END);

            ?>

        </div>
    </div>
</div>
    <div class="clear"></div>
    <div class="description_tab">
        <div class="description_tab">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Описание</a></li>
                    <li><a href="#tab2" data-toggle="tab">Оптовикам</a></li>
                    <li><a href="#tab3" data-toggle="tab">Доставка</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <h4><?=$product->name?></h4>
                        <p>
                            <?=$product->description?>
                        </p>
<!--                        <a class="mini_cart_right" href="#">в корзину</a>-->
                        <div class="clear"></div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <h4>Скидки оптовым покупателям</h4>
                        <p>
                            Действуют оптовые цены.
                            Для получения детальной информации по вопросам оптовых закупок свяжитесь с нашим менеджером по телефону (0482) 31-31-55.
                        </p>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <h4>Стоимость доставки</h4>
                        <img src="/img/dost2.png"/>
                        <p>
                            Доставка заказа осуществляется во все действующие отделения «Новой почты» на территории Украины.<br>
                            Доставка занимает от 1 до 3 дней. После оформления заказа на сайте наши менеджеры перезвонят Вам и согласуют дату доставки заказа перед отправкой.<br><br>
                            Более подробную информацию о графике работы и стоимости доставки Вы можете узнать на сайте «Новой почты» - <a target="_blank" href="http://novaposhta.ua/.">novaposhta.ua</a><br>
                            Оплата заказа производится в отделении или курьеру компании «Нова пошта».<br>
                        </p>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    <div class="similar_products">
<!--        <h4>Похожие товары:</h4>-->
        <div class="similar_products_item">
            <?  if(isset($productPopular) && count($productPopular)>0){ foreach($productPopular as $p)
            {
            ?>
                <div class="similar_products_block"><a href="<?=\yii\helpers\Url::toRoute(['catalog/view','id'=>$p->id])?>">
<?
                        echo  EasyThumbnailImage::thumbnailImg(
                            $p->images[0]->getPath(),
                            500,
                            700,
                            EasyThumbnailImage::THUMBNAIL_INSET,
                            ['alt' => $p->id]
                            );
//                    echo $p->images[0]->getPath();



?>
                </a></div>
            <?}}?>
        </div>
        <div class="clear"></div>


    </div>


</div>
</div>
</div>
