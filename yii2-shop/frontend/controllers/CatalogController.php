<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Goods;
use common\models\Product;
use common\models\Params;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Response;
use Yii;

class CatalogController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }

    public function actionList($id = null)
    {
//        \Yii::$app->cart->removeAll();
        /** @var Category $category */
        $category = null;


        $categories = Category::find()->indexBy('id')->orderBy('id')->all();


        $productsQuery = Goods::find();
        if ($id !== null && isset($categories[$id])) {
            $category = $categories[$id];
            $productsQuery->where(['category_id' => $this->getCategoryIds($categories, $id)]);
        }
        else
        {
            $productsQuery->andWhere(['popular'=>1]);
        }

        $productsQuery->andWhere(['status'=>Goods::STATUS_ACTIVE]);

        $productsDataProvider = new ActiveDataProvider([
            'query' => $productsQuery,
            'pagination' => [
                'pageSize' => ($id!=null ? 50 : 8),
            ],
        ]);


        return $this->render('list', [
            'category' => $category,
            'menuItems' => $this->getMenuItems($categories, isset($category->id) ? $category->id : null),
            'productsDataProvider' => $productsDataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model=new Product(['scenario'=>'addToCard']);

        $model->goods_id=$id;

        $product=Goods::findOne($id);
        if($product->status==0)
        {
            return $this->redirect(['/']);
        }
        $productPopular=Goods::find()->where(['popular'=>1,'status'=>Goods::STATUS_ACTIVE])->orderBy('RAND()')->limit(5)->all();



        $model->load(Yii::$app->request->post());

        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $item=Product::find()->where(['goods_id'=>$id,'color'=>$model->color,'size'=>$model->size])->one();
        if($item->sku)
        {
            return $this->redirect(['cart/add', 'id' => $item->id]);
        }

        $prev = Goods::find()->where(['<','id',$id])->andWhere(['status'=>Goods::STATUS_ACTIVE])->one();
        $next = Goods::find()->where(['>','id',$id])->andWhere(['status'=>Goods::STATUS_ACTIVE])->one();;
        $categories = Category::find()->indexBy('id')->orderBy('id')->all();

        return $this->render('view',[
            'menuItems' => $this->getMenuItems($categories, isset($category->id) ? $category->id : null),
            'product' => $product,
            'productPopular' => $productPopular,
            'model' => $model,
            'colors'=>$product->colors,
            'sizes'=>$product->sizes,
            'prev' => $prev,
            'next' => $next,
            ]
    );
    }

    public function actionFilter($id)
    {
        $model=new Product(['scenario'=>'addToCard']);

        $model->load(Yii::$app->request->post());
        $model->goods_id=$id;
        //colors

        if($model->color!='')
        {
            $colors=Product::find()->select('size')->distinct('size')->where(['color'=>$model->color,'goods_id'=>$id])->andWhere(['>','sku','0'])->all();
        }
        else
        {
            $colors=Product::find()->select('size')->distinct('size')->where(['goods_id'=>$id])->andWhere(['>','sku','0'])->all();
        }

        foreach($colors as $c)
        {
            $tmpC[]=$c->size;

        }
        //sizes

        if($model->size!='')
        {
            $sizes=Product::find()->select('color')->distinct('color')->where(['size'=>$model->size,'goods_id'=>$id])->andWhere(['>','sku','0'])->all();

        }
        else
        {
            $sizes=Product::find()->select('color')->distinct('color')->where(['goods_id'=>$id])->andWhere(['>','sku','0'])->all();
        }
        foreach($sizes as $c)
        {
            $tmpS[]=$c->color;

        }
//        $colors=Params::find()->where(['id'=>$tmpS])->asArray()->all();
        $colors=Params::find()->where(['id'=>$tmpS])->all();
//        $sizes=Params::find()->where(['id'=>$tmpC])->asArray()->all();
        $sizes=Params::find()->where(['id'=>$tmpC])->all();
//        print_r($colors);
//        print_r($sizes);
//        exit;


            return $this->renderPartial('filterAttr2',[
                'changeAttr'=>$changeAttr,
                'res'=>$res,
                'colors'=>$colors,
                'sizes'=>$sizes,
                'model'=>$model,
            ]);
    }

    public function activeValidation()
    {

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $errors=ActiveForm::validate($model);
            return $errors;
        }
    }

    /**
     * @param Category[] $categories
     * @param int $activeId
     * @return array
     */
    private function getMenuItems($categories, $activeId = null)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            if ($category->parent_id === 0) {
                $menuItems[$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/list', 'id' => $category->id],
                ];
            } else {
                $this->placeCategory($category, $menuItems, $activeId);
            }
        }
        return $menuItems;
    }

    /**
     * Places category menu item into menu tree
     *
     * @param Category $category
     * @param $menuItems
     * @param int $activeId
     */
    private function placeCategory($category, &$menuItems, $activeId = null)
    {
        foreach ($menuItems as $id => $navLink) {
            if ($category->parent_id === $id) {
                $menuItems[$id]['items'][$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/list', 'id' => $category->id],
                ];
                break;
            } elseif (!empty($menuItems[$id]['items'])) {
                $this->placeCategory($category, $menuItems[$id]['items']);
            }
        }
    }

    /**
     * Returns IDs of category and all its sub-categories
     *
     * @param Category[] $categories all categories
     * @param int $categoryId id of category to start search with
     * @param array $categoryIds
     * @return array $categoryIds
     */
    private function getCategoryIds($categories, $categoryId, &$categoryIds = [])
    {
        foreach ($categories as $category) {
            if ($category->id == $categoryId || $category->parent_id == $categoryId) {
                $categoryIds[] = $category->id;
            }
            if (isset($categories[$categoryId]['items'])) {
                foreach ($categories[$categoryId]['items'] as $subCategoryId => $subCategory)
                $this->getCategoryIds($categories, $subCategoryId, $categoryIds);
            }
        }
        return $categoryIds;
    }
}
