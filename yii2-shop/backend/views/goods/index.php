<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use common\models\Category;
use common\models\Goods;
use backend\models\GoodsSearch;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Goods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Goods', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute'=>'status',
                'filter'=>Html::activeDropDownList(new GoodsSearch(),'status',ArrayHelper::merge([''=>'Select all'],Goods::getStatuses()),['class'=>'form-control','prompt' => 'Select status']),
                'value'=> function($model)
                    {
                        return $model->realStatus;
                    }
            ],[
                'attribute'=>'popular',
                'filter'=>Html::activeDropDownList(new GoodsSearch(),'popular',ArrayHelper::merge([''=>'Select all'],[0=>'Нет',1=>'Да',]),['class'=>'form-control','prompt' => 'Select popular']),
                'value'=> function($model)
                    {
                        return ($model->popular==0 ? 'Нет' : 'Да' );
                    }
            ],
            'order',
            [
                'attribute' => 'category_id',
                'filter'=>Html::activeDropDownList(new GoodsSearch(),'category_id',ArrayHelper::merge([''=>'Select all'],ArrayHelper::map(Category::find()->where(['>','parent_id','0'])->asArray()->all(), 'id', 'title')),['class'=>'form-control','prompt' => 'Select category']),
                'value' => function ($model) {
                        return empty($model->category_id) ? '-' : $model->category->title;
                    },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

