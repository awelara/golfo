<?php
use yii\helpers\Html;
use yii\helpers\Markdown;
use himiklab\thumbnail\EasyThumbnailImage;
?>
<?php /** @var $model \common\models\Product */ ?>



<div class="goods1 span3">
    <a href="<?=\yii\helpers\Url::toRoute(['catalog/view','id'=>$model->id])?>"><div class="goods_wrap_img">
            <?php
            $images = $model->images;
            if (isset($images[0])) {
                echo  EasyThumbnailImage::thumbnailImg(
                    $images[0]->getPath(),
                    500,
                    700,
                    EasyThumbnailImage::THUMBNAIL_INSET,
                    ['alt' => $images[0]->id]
                );
            }
            ?>
        </div>
    </a>
    <div class="price">
        <div class="price_mini">
            <p><?= Html::encode(round($model->product->price,2)) ?> грн.</p>
            <p><span>в наличии</span></p>
        </div>
        <div class="cart_buttun">
            <a href="<?=\yii\helpers\Url::toRoute(['catalog/view','id'=>$model->id])?>"> Подробнее</a>
        </div>
        <div class="clear"></div>
        <div class="description">
            <p><?=$model->description?></p>
        </div>
    </div>
</div>