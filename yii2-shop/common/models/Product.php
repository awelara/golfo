<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $category_id
 * @property string $price
 *
 * @property Image[] $images
 * @property OrderItem[] $orderItems
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ]
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['category_id','num','goods_id','size','color'], 'integer'],
            [['goods_id','size','color'], 'required','on'=>['addToCard'],'message'=>'Выберите {attribute}'],
            [['price','sku'], 'number'],
            [['title'], 'string', 'max' => 255]
        ];
    }



    public function afterValidate()
    {
        $sku=Product::findOne(['goods_id'=>$this->goods_id,'color'=>$this->color,'size'=>$this->size]);

//        $this->id=$sku->id;

        if($sku->sku && $this->color && $this->size)
        {
//            $this->addError('sku', 'Артикул: '.$sku->sku);

//                \Yii::$app->cart->put($sku);

        }
        $this->trigger(self::EVENT_AFTER_VALIDATE);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sku' => 'sku',
            'slug' => 'Slug',
            'description' => 'Description',
            'category_id' => 'Category ID',
            'price' => 'Price',
            'size' => 'Размер',
            'color' => 'Цвет',
            'num' => 'Num',
        ];
    }

    /**
     * @return Image[]
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getGoods()
    {
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }

    public function getColors()
    {
        return $this->hasOne(Params::className(), ['color' => 'id']);
    }
    public function getSizes()
    {
        return $this->hasOne(Params::className(), ['size' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getPrice()
    {
        return round($this->price,2);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
}
