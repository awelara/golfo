<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log','thumbnail'],
    'modules' => [],
    'defaultRoute' => 'order/index',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
          'thumbs/<path:.*>' => 'image/thumb',
        ],
      ],
      'thumbnail' => [
                'class' => 'himiklab\thumbnail\EasyThumbnail',
                'cacheAlias' => 'assets/thumbs',
            ],
        'imageCache' => [
            'class' => 'iutbay\yii2imagecache\ImageCache',
            'sourcePath' => '@frontendFiles/files',
            'sourceUrl' => '@frontendWebroot/files',
            'thumbsPath' => '@frontendFiles/thumbs',
            'thumbsUrl' => '@frontendWebroot/thumbs',
            'sizes' => [
                'thumb' => [150, 150],
                'medium' => [300, 300],
                'large' => [600, 600],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
