<?php
/* @var $this yii\web\View */
use himiklab\thumbnail\EasyThumbnailImage;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


?>


<br>
<div class="clear"></div>
</div>


<div class="card_product">
<div class="container">
<div class="block_left span3">

        <?
        echo \frontend\widgets\MenuBase::widget();
        ?>
    <div class="clear"></div>


</div>
<div class="block_right span9">
<h1>
<?=$page->title?>
</h1>
<div class="main_left span8">

    <div id="main" role="main">
        <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        }
        ?>
        <?=$page->text?>
        <? if($form!='') include(__DIR__.'/contactForm.php')?>
    </div>
</div>

    <div class="clear"></div>
</div>
</div>
