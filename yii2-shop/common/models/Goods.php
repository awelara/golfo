<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order
 * @property integer $category_id
 */
class Goods extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = '1';
    const STATUS_NOT_ACTIVE = '0';

    public static function getStatuses()
    {
        return [
            1 => 'Активный',
            0 => 'Не активный',
        ];
    }

    public function getrealStatus()
    {
        $statuses=$this->getStatuses();
        return $statuses[$this->status];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['order', 'category_id','popular',], 'integer'],
            [['name' ], 'string', 'max' => 255],
            [['description' ], 'string', 'max' => 512],
            [['status'], 'string', 'max' => 55]
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }


    public function getColors()
    {
        return $this->hasMany(Params::className(), ['id' => 'color'])
            ->viaTable('product',['goods_id'=>'id']);
    }

    public function getSizes()
    {
        return $this->hasMany(Params::className(), ['id' => 'size'])
            ->viaTable('product',['goods_id'=>'id']);
    }
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['product_id' => 'id'])
            ->viaTable('product',['goods_id'=>'id']);
    }




    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['goods_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order' => 'Order',
            'status' => 'Status',
            'category_id' => 'Category ID',
        ];
    }
}
