<?php
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Menu;
use frontend\widgets\MenuBase;

/* @var $this yii\web\View */
$title = $category === null ? 'Популярные товары' : $category->title;
$this->title = Html::encode($title);
?>
<div class="container">
    <div class="seo_text"></div>
    <div class="clear"></div>
    <?if ($category === null){?>
    <div class="slider">
        <div id="da-slider" class="da-slider">
            <div class="da-slide">
                <h2>ГОЛЬФ ЖЕНСКИЙ</h2>
                <p>Наша торговая марка предлагает качественный трикотаж оптом и в розницу. У нас Вы найдете одежду на любой вкус, сможете оценить разнообразие цветовой гаммы и моделей.</p>
                <a href="http://golfo.com.ua/catalog/view?id=9" class="da-link">Купить</a>
                <div class="da-img"><img src="/img/T-shirt.png"></div>
            </div>
            <div class="da-slide">
                <h2>Футболки мужские</h2>
                <p>Оптом и в розницу. У нас низкие цены и большой выбор! Новая коллекция 2015.</p>
                <a href="http://golfo.com.ua/catalog/view?id=22" class="da-link">Купить</a>
                <div class="da-img"><img src="/img/T-shirt2.png"></div>
            </div>

            <nav class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>
            </nav>
        </div>
    </div>
    <?}?>


    <?

    $script = <<< JS
   $(function () {

            $('#da-slider').cslider({
                bgincrement: 0,
                autoplay: true
            });

        });
JS;
    $this->registerJs($script, View::POS_END);
    ?>

    <div class="clear"></div>
<div class="circles">

    <?

    echo \frontend\widgets\Menu::widget();
    ?>
</div>
<div class="clear"></div>
</div>

<div class="popular">
    <div class="container">
        <h1><?=$this->title?></h1>


          <?= ListView::widget([
              'dataProvider' => $productsDataProvider,
              'itemView' => '_product',
              'layout'=>'{items}'
          ]) ?>
</div>