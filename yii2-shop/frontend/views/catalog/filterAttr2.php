<?php
/**
 * Created by PhpStorm.
 * User: awelara
 * Date: 27.05.15
 * Time: 16:38
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


//print_r($res);
//print_r($model);


$form = ActiveForm::begin([
    'id'=>'form-filter',
    'enableAjaxValidation' => true,
//                'enableClientValidation' => true,
    'validateOnSubmit'=>true,
    'validateOnChange'=>true,
]);
?>

<?= Html::submitButton(Yii::t('app', 'В корзину'), ['class' => 'mini_cart_right helpful6']) ?>

    <div class="clear"></div>

    <p class="in_stock">В наличии</p>
    <div class="description_right">
        <?= $form->field($model, 'sku')->label(false)->textInput(['maxlength' => 10,'style'=>'display:none']) ?>
    </div>

    <div class="select_color" >
        <h4>Выбор цвета:</h4>
        <ul>

            <?  foreach($colors as $c){
                ?><li><a id="product-color option[value='<?=$c->id?>']" dataid="<?=$c->id?>" style="background: <?=mb_strtolower($c->value)?>;" class="product-color <?=(($c->id==$model->color) ? "border" : "" )?>" ></a></li>
            <?
            } ?>
        </ul>
        <?= $form->field($model, 'color')->label(false)->dropDownList(ArrayHelper::map($colors, 'id', 'label'), ['prompt' => 'Select color','style'=>'display:none']) ?>
        <div class="clear"></div>
    </div>


    <div class="select_size" >
        <h4>Выбор размера:</h4>
        <ul>
            <?  foreach($sizes as $c){
                ?><li><a class="product-size <?=(($c->id==$model->size) ? "border-size" : "" )?>" id="product-size option[value='<?=$c->id?>']"  dataid="<?=$c->id?>" ><?=mb_strtolower($c->value)?></a></li>
            <?
            } ?>
        </ul>
        <?= $form->field($model, 'size')->label(false)->dropDownList(ArrayHelper::map($sizes, 'id', 'label'), ['prompt' => 'Select size','style'=>'display:none']) ?>

        <div class="clear"></div>
    </div>



<?php ActiveForm::end(); ?>