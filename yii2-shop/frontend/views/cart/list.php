<?php
use \yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $products common\models\Product[] */

?>

<div class="cart_issue">
    <table class="bodyTable">
        <thead>
        <tr>
            <td>Фото</td>
            <td>Наименование</td>

            <td>Количество</td>
            <td>Цена</td>
            <td>Итого</td>
            <td></td>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($products as $product):?>
            <tr>
                <td>
                    <?
//                    print_r($product->images);
                    $img=EasyThumbnailImage::thumbnailImg(
                            $product->goods->images[0]->getPath(),
                            55,
                            55,
                            EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                            ['alt' =>$product->goods->images[0]->id]);
                    ?>
                    <a href="<??>">

                        <?=$img?>
                    </a>
                </td>
                <td>
                    <a href="<?=\yii\helpers\Url::toRoute(['catalog/view','id'=>$product->goods_id])?>" class="url">
                        <?= Html::encode($product->title) ?> </a>
                </td>

                <td>


                    <div class="btn btn-small updateCart"  data_id="<?=$product->id?>" data_val="<?=($product->getQuantity()-1)?>">-</div>

<!--                    --><?//= Html::a('-', '#', ['class' => 'btn btn-small updateCart','data_val'=>($product->getQuantity()-1)])?>
                    <div class="count" contenteditable="true" ><?=$product->getQuantity()?></div>
                    <div class="btn btn-small updateCart" data_id="<?=$product->id?>" data_val="<?=($product->getQuantity()+1)?>">+</div>
<!--                    --><?//= Html::span('+', '#', ['class' => 'btn btn-small updateCart','data-val'=>($product->getQuantity()+1)])?>
<!--                    --><?//= Html::a('×', ['cart/remove', 'id' => $product->getId()], ['class' => 'remove'])?>
                </td>
                <td>
                     <span class="priceNum"><?= $product->Price ?> </span>грн.
                </td>
                <td>
                     <span class="totalNum"><?=$product->getCost()?> </span>грн.
                </td>
                <td>
                    <div class="btn btn-small deleteCart" data_val="<?=$product->id?>">x</div>
                </td>







            </tr>
        <?php endforeach ?>

        </tbody>
    </table>
    <div class="fullTotal">
        Сумма:
                        <span class="fullNum">
                             <?= $total ?>            </span>грн.
    </div>
</div>

<script>

    $('.updateCart').on('click',function(){
        $.ajax({
            type: "GET",
            url: "/cart/update/?id="+$(this).attr('data_id')+'&quantity='+$(this).attr('data_val'),
            success: function(msg){
//                alert(msg)
                $( "#cartFormItems").html(msg)
            }
        });
    });


    $('.deleteCart').on('click',function(){
        $.ajax({
            type: "GET",
            url: "/cart/remove/?id="+$(this).attr('data_val'),
            success: function(msg){
                $( "#cartFormItems").html(msg)
            }
            })
        });


</script>

